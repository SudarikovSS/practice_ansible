#!/bin/sh
export FLASK_APP=app.py
export POSTGRESQL_URL=postgresql://worker:worker@localhost/app
python3 -m flask db upgrade
python3 ~/for_ansible/app/app.py
